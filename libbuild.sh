#  ---  LIBBUILD.SH ---
# little library for building shell-scripts

# DIRECTORIES

root_dir=.
src_dir=./src
bin_dir=./bin
debug_bin_dir=./bin/debug
release_bin_dir=./bin/release

# COMPILER KEYS

all_max_optimize="-O3"      # maximum optimization
all_debug_key="-g"          # adding debug info and DEBUG define
fpc_def_debug="-dDEBUG"     # FPC - adding DEBUG preprocessor define
gcc_all_warn="-Wall"        # GCC - output all warnings

# OTHER

check_err_pause=2

# -----------------
# --- functions ---
# -----------------

print_result()
{
  if [ $? -eq 0 ]; then
    echo -e "\e[32mOK\e[0m"
  else
    echo -e "\e[31mnot OK...\e[0m"
  fi
}

create_bin_dirs()
{
  echo
  if [ -d $bin_dir ]; then
    :
  else
    echo -ne "\e[0mCreating \e[1;33m$bin_dir\e[0m directory... "
    mkdir $bin_dir
    print_result
  fi

  if [ -d $debug_bin_dir ]; then
    :
  else
    echo -ne "Creating \e[1;33m$debug_bin_dir\e[0m directory... "
    mkdir $debug_bin_dir
    print_result
  fi

  if [ -d $release_bin_dir ]; then
    :
  else
    echo -ne "Creating \e[1;33m$release_bin_dir\e[0m directory... "
    mkdir $release_bin_dir
    print_result
  fi

  echo -e "\e[0m"
}

check_compile_results() {
  if [ $? != 0 ]; then
    echo -e "\e[1;31mCOMPILE ERRORS...\e[0m\n"
    read -s -n 1 -t $check_err_pause -p "Press [any key] ..."
    echo
    return 1
  else
    echo -e "\e[1;32mCOMPILE SUCCESSFULLY !\e[0m"
    return 0
  fi
}
