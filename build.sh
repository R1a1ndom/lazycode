#!/bin/bash

# LAZYCODE build script

source libbuild.sh

act_debug_build=1
act_release_build=2
act_clear_dirs=3

write_choice_prompt()
{
  echo -e "\e[1;34m---------------------------------------------"
  echo -e "\e[35mLAZYCODE utility build script"
  echo -e "\e[0;32mChoose an action:\e[0m"
  echo -e "\e[1;34m---------------------------------------------\e[0m"
  echo -e "\e[1;33m[ 1 ]\e[0m build DEBUG version"
  echo -e "\e[1;33m[ 2 ]\e[0m build RELEASE version"
  echo -e "\e[1;33m[ 3 ]\e[0m CLEAR directories from temporary files"
  echo -e "\e[1;33m[ any key ]\e[0m to EXIT"
  echo -ne "\e[1;34m---------------------------------------------\e[0m"
}

gcc_compile_one_file() # $1 source file  $2 directory  $3 keys
{
  echo "| filename: $1 | directory: $2 | keys: $3 $4 |"
  echo -en "Compiling \e[1;33m$1\e[0m ... "
  gcc $3 $4 -c $2/$1
  print_result
  return
}

purge_dir()
{
  rm $1/*.o 2>/dev/null
  rm $1/*~ 2>/dev/null
  rm $1/lazycode 2>/dev/null
}

# --- --- --- --- --- ---
# ---   MAIN SCRIPT   ---
# --- --- --- --- --- ---

write_choice_prompt
read -n 1 action
create_bin_dirs
case $action in
  $act_debug_build)
    echo -e "\e[1;34mBuilding DEBUG version:\e[0m"

    counter=0;
    gcc_compile_one_file lazycode.c $src_dir $all_debug_key $gcc_all_warn
    if [ $? -eq 0 ]; then
      counter=$(($counter+1))
    fi
    gcc_compile_one_file write_msg.c $src_dir $gcc_debug_keys 
    if [ $? -eq 0 ]; then
      counter=$(($counter+1))
    fi

    gcc_compile_one_file main.c $src_dir $gcc_debug_keys 
    if [ $? -eq 0 ]; then
      counter=$(($counter+1))
    fi

    echo "Counter = $counter"

    if [ $counter -eq 3 ]; then
      echo -n "Linking ..."
      cd $src_dir
      gcc $debug_keys -o ../$debug_bin_dir/lazycode main.o write_msg.o lazycode.o
      print_result
    else
      echo -e "\e[1;34mNothing to linking.\e[0m"
    fi
  ;;
  $act_release_build)
     echo "not released..."
  ;;

  $act_clear_dirs)
    echo -e "Following files will br deleted:\e[1;33m"
    echo $(ls -1 $src_dir/*.o $src_dir/*~ 2>/dev/null)
    echo $(ls -1 $debug_bin_dir/lazycode 2>/dev/null)
    echo $(ls -1 $debug_bin_dir/lazycode 2>/dev/null)
    purge_dir ./
    purge_dir $src_dir
    purge_dir $debug_bin_dir
    purge_dir $release_bin_dir
    echo -e "\e[0;3mR E A D Y !\e[0m"
  ;;
  *)
    echo "Exit from script."

  ;;

esac