/*

   LAZYCODE.H - part of LAZYCODE utility

 */

#ifndef LAZYCODE_H
#define LAZYCODE_H

static const char opt_string[]   = ":t:l:h?";

static const char ext_c[]        = ".c";
static const char ext_c_header[] = ".h";
static const char ext_pascal[]   = ".pas";

static const char option_new[]    = "new";
static const char option_unit[]   = "unit";
static const char option_c[]      = "c";
static const char option_pascal[] = "pascal";

enum {
  NODEF,            // new project
  TYPE_NEW,           // program unit
  TYPE_UNIT           // not defined
  LANG_C,
  LANG_PASCAL
} ;

typedef struct program_task {
  int template_type;
  int language_type;
} program_task;

/* --- --- functions --- --- */

template_type str2template(const char* our_str);

language_type str2language(const char* our_str);

#endif