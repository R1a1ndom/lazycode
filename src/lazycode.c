/*

   LAZYCODE.C - part of LAZYCODE utility

 */
 
#include <string.h>
#include "lazycode.h"

template_type str2template(const char* our_str)
{
  if (strcmp(our_str,option_new) == 0) {
    return type_new;
  } else if (strcmp(our_str,option_unit) == 0) {
    return type_unit;
  } else
    return type_nodef;
}

language_type str2language(const char* our_str)
{
  if (strcmp(our_str,option_c) == 0) {
    return lang_c;
  } else if (strcmp(our_str,option_pascal) == 0) {
    return lang_pascal;
  } else
    return lang_nodef;
}