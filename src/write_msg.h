/*

   WRITE_MSG.H  ---  part of LAZYCODE utility

 */

#ifndef WRITE_MSG_H
#define WRITE_MSG_H

void write_title();

void write_full_help();

void print_lang_choose(enum language_type lang);

void print_template_choose(enum template_type template);

int print_parse_results(const *program_task task);

#endif
