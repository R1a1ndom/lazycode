/*

    MAIN.C  ---  part of LAZYCODE utility (main file)

 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "lazycode.h"
#include "write_msg.h"


int main(int argc, char* argv[])
{
  int next_option;
  program_task main_task;
  main_task.template = type_nodef;
  main_task.language = lang_nodef;
  while ((next_option = getopt(argc,argv,opt_string)) != -1  )
  {
    switch (opt) {
      case 'h':
        write_title();
        write_full_help();
        break;
      case 't':

      case 'l':
 
 
      case '?':        
        write_title();
        printf("type lazycode -h for help\n");
        break;
      default:
        printf("Unknown argument or option\n");
        break;
    }  
  }
  return 0;
}