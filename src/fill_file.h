/*

   FILL_FILE.H - part of LAZYCODE utility 
 
 */

#ifndef FILL_FILE_H
#define FILL_FILE_H

void write_c_main_template(const char* name, FILE* stream);

void write_c_makefile(const char* name, FILE* stream);

#endif