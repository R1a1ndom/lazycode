/*

   WRITE_MSG.C  ---  part of LAZYCODE utility

 */


#include <stdio.h>
#include "write_msg.h"
#include "lazycode.h"

void write_title()
{
  printf("---> LAZYCODE utility <---\n");
  printf("creating source file templates\n");
  printf("Usage:\n");
  printf("$ lazycode -l <language> -t <template> <name>\n");
}

void write_full_help()
{
  printf("-l <language> :\n-------------\n");
  printf("c - C Language\n");
  printf("pas - Free/Turbo Pascal\n");
  printf("-t <template type> :\n-------------\n");
  printf("new - New project\n");
  printf("unit - One unit: *.h / *.c or *.pas file\n");
  printf("-h -? Write this help message\n");

}

void print_lang_choose(enum language_type lang)
{
  switch (lang)
  {
    case lang_c:
      printf("You chosen C language\n");
      break;
    case lang_pascal:
      printf("You chosen PASCAL language\n");
      break;
    case lang_nodef:
      printf("Language: argument required!\n");
      break;
  }
}

void print_template_choose(enum template_type template)
{
  switch (template)
  {
    case type_new:
      printf("You chosen NEW PROJECT.\n");
      break;
    case type_unit:
      printf("You chosen ONE UNIT.\n");
      break;
    case type_nodef:
      printf("Template: argument required!\n");
      break; 
  }
}


int print_parse_results(const *program_task task)
{
  return 0;
}
